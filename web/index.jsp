<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<%--
    This file is an entry point for JavaServer Faces application.
--%>

    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>JSF SQL GATEWAY</title>
        </head>
        <body>
            <c:if test="${sqlStatement == null}" >
                <c:set var="sqlStatement" value="select * from user_details"/>
            </c:if>
        
            <h1>The SQL Gateway</h1>
            <p>enter an SQL statement and click the execute button. Then, information about the <br>
            statement will appear at the bottom of this page</p>


                <b>SQL statement:</b><br>
            <form action="SqlGatewayServlet" method="post">
                <textarea name="sqlStatement" cols="60" rows="8">${sqlStatement}
                </textarea><br><br>
                <input type="submit" value="Execute">
            </form>

            <p>
            <b>SQL Result:</b><br>
             ${sqlResult}
            </p>
        </body>
    </html>
