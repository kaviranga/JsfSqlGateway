/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.app;

/**
 *
 * @author kaviranga
 */

import java.util.*;
import java.sql.*;

public class SQLUtil {
    static StringBuffer htmlRows;
    static ResultSetMetaData metaData;
    static int columnCount;
    static int i;
            
    public static String getHtmlTable(ResultSet results) throws SQLException{
        htmlRows = new StringBuffer();
        metaData = results.getMetaData();
        columnCount =  metaData.getColumnCount();
        
        htmlRows.append("<table cellpadding=\"5\" border=\"1\">");
        htmlRows.append("<tr>");
        for(i=1;i<=columnCount;i++){
            htmlRows.append("<td><b>"+ metaData.getColumnName(i)+"</b></td>");
            htmlRows.append("</tr>");
            while(results.next()){
                htmlRows.append("<tr>");
                for(i=1;i<=columnCount;i++){
                    htmlRows.append("<td>"+results.getString(i)+"</td>");
                }
            }
            htmlRows.append("</tr>");
            htmlRows.append("</table>");
        }
        return htmlRows.toString();
    }
}