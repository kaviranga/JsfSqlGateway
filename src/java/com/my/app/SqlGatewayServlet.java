/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.app;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;
import com.mysql.jdbc.Connection;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kaviranga
 */
public class SqlGatewayServlet extends HttpServlet {

    private static final long serialVersionUID = -256184605231791947L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
        Connection con;
        Statement stmt;
        ResultSet rs;
        String sqlStatement;
        String sqlResult="";
        String sqlType;
        String url;
        int i;
        RequestDispatcher dispatcher;
        HttpSession session;
    
    /*protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        
    }*/

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        sqlStatement = request.getParameter("sqlStatement");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //get a connection
            String DbUrl = "jdbc:mysql://localhost:3306/user_info";
            String username = "root";
            String password = "";
            con = (Connection)DriverManager.getConnection(DbUrl,username,password);
            
            //create statement
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            
            //parse the sql string
            sqlStatement = sqlStatement.trim();
            if(sqlStatement.length()>=6){
                 sqlType = sqlStatement.substring(0,6);
                    if(sqlType.equalsIgnoreCase("select")){
                        //create the resultset
                        rs = stmt.executeQuery(sqlStatement);
                        sqlResult = SQLUtil.getHtmlTable(rs);
                        rs.close();
                    }else{
                         i = stmt.executeUpdate(sqlStatement);
                         if(i==0)
                             sqlResult = "The statement executed successfully.";
                         else
                             sqlResult = "The statement executed successfully.<br>" + i + "row(s) affected";
                    }
                stmt.close();
                con.close();
            }
        } catch (SQLException ex) {
            sqlResult =" Error executing the sql statement: <br>" + ex.getMessage();
            
        } catch (ClassNotFoundException ex) {
            System.out.println(""+ex.getMessage());
        }
        
        session = request.getSession();
        session.setAttribute("sqlResult",sqlResult);
        session.setAttribute("sqlStatement",sqlStatement);
        
        url = "/index.jsp";
        dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Sql Gateway Servlet";
    }// </editor-fold>
}