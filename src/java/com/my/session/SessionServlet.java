/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.session;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kaviranga
 */
public class SessionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
	
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Set the response message's MIME type	
			response.setContentType("text/html;charset=UTF-8");
			// Allocate a output writer to write the response message into the network socket
			PrintWriter out = response.getWriter();
			
			// Return the existing session if there is one. Create a new session otherwise.
			HttpSession session = request.getSession();
			Integer accessCount;
			synchronized(session){
				accessCount = (Integer)session.getAttribute("accessCount");
				if (accessCount == null) {
					accessCount = 0; // autobox int to Integer
				}else{
					accessCount = new Integer(accessCount + 1);
				}
				
			}
			session.setAttribute("accessCount", accessCount);
                        
                        try{					
				out.println("<!DOCTYPE html>");
				out.println("<html>");
				out.println("<head><meta http‐equiv='Content‐Type' content='text/html; charset=UTF‐8'>");
				out.println("<title>Session Test Servlet</title></head><body>");
				out.println("<h2>You have access this site " + accessCount + " times in this session.</h2>");
				out.println("<p>(Session ID is " + session.getId() + ")</p>");
				out.println("<p>(Session creation time is " +
					new Date(session.getCreationTime()) + ")</p>");
				out.println("<p>(Session last access time is " +
					new Date(session.getLastAccessedTime()) + ")</p>");
				out.println("<p>(Session max inactive interval is " +
					session.getMaxInactiveInterval() + " seconds)</p>");
				out.println("<p><a href='" + request.getRequestURI() + "'>Refresh</a>");
				out.println("<p><a href='" + response.encodeURL(request.getRequestURI()) +"'>Refresh with URL rewriting</a>");
				out.println("</body></html>");
			}finally {
				out.close(); // Always close the output writer
			}        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}